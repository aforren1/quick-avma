from timeclock import TimeClock
import csv
import os
import sys
from collections import OrderedDict
from datetime import datetime

import yaml
from toon.audio import beep_sequence
from toon.input import MultiprocessInput as MpI
from toon.input.clock import mono_clock
from devices.keyboard import Keyboard

from mk_blocks import mk_table
from psychopy import core, event, gui, logging, visual
from psychopy.preferences import prefs
from psychopy.visual.rect import Rect

prefs.general['audioLib'] = ['sounddevice']


def empty_trial_resp(lst):
    del lst[:]


def reschedule_timer(timer, new_val):
    timer.reset(new_val)


def save_img_time(dct):
    # temporarily store time of image in here
    dct['prep_time'] = trial_timer.getTime()


if __name__ == '__main__':
    # settings
    settings = OrderedDict({'subject': '001', 'block': '1', 'free_resp': False, 'nogo': False})
    try:
        with open('user_settings.yml', 'r') as f:
            pot_sets = yaml.load(f)
        if pot_sets.keys() == settings.keys():
            for i in settings.keys():
                settings[i] = pot_sets[i]
    except (FileNotFoundError, AttributeError):
        pass

    dlg = gui.DlgFromDict(settings, title='Experiment')
    if not dlg.OK:
        sys.exit()

    settings = dict(settings)
    with open('user_settings.yml', 'w') as f:
        yaml.dump(settings, f, default_flow_style=False)

    subject = settings['subject']
    block = settings['block']
    free_resp = settings['free_resp']
    nogo = settings['nogo']
    ignore = [0, 2]  # ignore index and ring

    fstr = 'free' if free_resp else 'forced'
    seed = int(datetime.now().strftime('%H%M%S'))
    data_path = os.path.join('data', subject, 'block%s_%s_%i.csv' % (block, fstr, seed))
    interval = 1/60
    #keys = ['H', 'U', 'I', 'L']
    keys = ['h', 'u', 'i', 'l']
    symbs = ['\u16A0', '\u16a2', '\u16a3', '\u16a6']

    # not settings
    if not os.path.exists('data'):
        os.mkdir('data')

    if not os.path.exists(os.path.join('data', subject)):
        os.mkdir(os.path.join('data', subject))

    trial_data = dict.fromkeys(['subject', 'block', 'type', 'req_prep_time', 'prep_time',
                                'want', 'resp', 'nogo', 'nogo_this', 'correct'])

    trial_data['subject'] = subject
    trial_data['block'] = block
    trial_data['type'] = fstr
    trial_data['nogo'] = int(nogo)

    with open(data_path, 'w') as f:
        writer = csv.DictWriter(f, fieldnames=trial_data.keys())
        writer.writeheader()

    # graphics
    win = visual.Window(winType='glfw', fullscr=True, allowGUI=False,
                        size=(1920, 1080), color=-1, units='height')

    intro_text = visual.TextStim(win, text='Press [Space] to start.',
                                 pos=(0, 0), units='height',
                                 color=1, height=0.08,
                                 alignHoriz='center', alignVert='center',
                                 autoLog=False)

    push_feedback = Rect(win, width=0.6, height=0.6, lineWidth=3, autoLog=False)

    # stimuli
    # text, but use as BufferImageStim for performance (maybe?)
    stim_list = []
    for i in symbs:
        stim_list.append(visual.TextStim(win, i, height=0.4,
                                         name='stim %s' % i, font='FreeMono'))

    # audio -- use coin to try and derive the latency
    # so the center of the last beep should be last_beep_time *after* sound onset
    # use if True to hack & make sure sound is imported *after* prefs are set
    if True:
        from psychopy import sound
    coin = sound.Sound('coin.wav')
    _beeps = beep_sequence(click_freq=[523.251, 659.255, 783.991, 1046.5],
                           inter_click_interval=0.4,
                           dur_clicks=0.04,
                           lead_in=0.1 - coin.stream.latency,
                           sample_rate=44100)
    coin = sound.Sound('coin.wav')
    last_beep_time = round(0.1 + 0.4 * 3, 2)
    beeps = sound.Sound(_beeps, hamming=False)

    # data
    tbl = mk_table(seed=seed, interval=interval)
    # helpers
    state = 'intro'
    any_pressed = 0  # ought to be >= 0
    trial_timer = core.Clock()
    tmp = TimeClock()
    trial_resps = []
    first_resp = None
    correct = False
    logged = False

    # get input chugging
    #keyboard = Keyboard(keys)
    keyboard = MpI(Keyboard, keys=keys, clock=mono_clock.get_time)
    keyboard.start()
    keyboard_state = [False] * len(keys)
    trial_start = 0

    while True:
        # read input
        timestamp, data = keyboard.read()
        if timestamp is not None:
            for i, j in zip(data[0], data[1]):
                keyboard_state[j[0]] = i[0]
            any_pressed = any(keyboard_state)

        push_feedback.lineColor = -0.2 if any_pressed else 1

        if state == 'intro':
            # move into trial
            if event.getKeys(['space']):
                state = 'pretrial'
            else:
                # not moving to the trial, keep doing the old thing
                intro_text.draw()

        if state == 'pretrial':
            # do stuff for the first frame of trial
            state = 'trial'
            try:
                current_trial_settings = tbl.pop(0)
                current_choice = current_trial_settings['choice']
                current_pt = current_trial_settings['prep_time']
            except IndexError:
                break
            if not free_resp:
                win.callOnFlip(beeps.play)
            win.callOnFlip(trial_timer.reset, 0)
            trial_start = mono_clock.get_time()
            trial_timer.reset(0)  # reset once already so that first doesn't eval early
            tmp.reset()
            empty_trial_resp(trial_resps)
            first_resp = None
            logged = False

        if state == 'trial':
            # start beeps
            # show image at some point
            if timestamp is not None:
                for i in range(len(data[0][0])):
                    if data[0][0][i]:
                        trial_resps.append({'key': data[2][0][i].decode('utf-8'),
                                            'time': timestamp[i] - trial_start})
            if free_resp:
                if nogo:
                    if trial_resps:
                        if not first_resp:
                            first_resp = trial_resps[0]
                        correct_choice = keys.index(trial_resps[0]['key']) == current_choice
                        correct = correct_choice and current_choice not in ignore
                        trial_timer.reset(0)
                        stim_list[current_choice].color = (-1, 1, -1) if correct else (1, -1, -1)
                        if correct:
                            coin.play()
                        state = 'feedback'
                    elif trial_timer.getTime() + interval >= 1.0:  # timeout
                        first_resp = {'key': None, 'time': None}
                        correct = current_choice in ignore  # wrong if late resp & not supposed
                        trial_timer.reset(0)
                        stim_list[current_choice].color = (-1, 1, -1) if correct else (1, -1, -1)
                        if correct:
                            coin.play()
                        state = 'feedback'
                    else:
                        stim_list[current_choice].draw()

                else:  # not nogo
                    if trial_resps:
                        if not first_resp:
                            first_resp = trial_resps[0]
                        # check correctness here
                        correct = keys.index(trial_resps[-1]['key']) == current_choice
                        trial_timer.reset(0)
                        stim_list[current_choice].color = (-1, 1, -1) if correct else (1, -1, -1)
                        if correct:
                            coin.play()
                        state = 'feedback'
                    else:
                        stim_list[current_choice].draw()

            else:  # forced resp
                tme = trial_timer.getTime()
                if tme + interval >= last_beep_time + 0.2:
                    beeps.stop()
                    if trial_resps:
                        first_resp = trial_resps[0]
                        good_timing = abs(first_resp['time'] - last_beep_time) <= 0.035
                        if nogo and current_choice in ignore:
                            correct = first_resp is None
                        else:
                            correct = keys.index(trial_resps[0]['key']) == current_choice
                        stim_list[current_choice].color = (-1, 1, -1) if correct else (1, -1, -1)
                    else:
                        first_resp = {'key': None, 'time': None}
                        if nogo and current_choice in ignore:
                            # handle case where
                            good_timing = True
                            correct = True
                            stim_list[current_choice].color = (-1, 1, -1)
                        else:
                            good_timing = False
                            correct = False
                            stim_list[current_choice].color = (1, -1, -1)
                    if good_timing and correct:
                        coin.play()
                    # set stimulus color & play sounds
                    trial_timer.reset(0)
                    state = 'feedback'
                else:
                    if tme + interval >= last_beep_time - current_pt:
                        stim_list[current_choice].draw()
                        if not logged:
                            logged = True
                            win.callOnFlip(save_img_time, trial_data)

        if state == 'feedback':
            if free_resp:
                if nogo:
                    if correct:
                        if trial_timer.getTime() + interval >= 0.3:
                            state = 'post'
                            trial_timer.reset(0)
                            stim_list[current_choice].color = (1, 1, 1)
                            with open(data_path, 'a') as f:
                                writer = csv.DictWriter(f, fieldnames=trial_data.keys())
                                trial_data['req_prep_time'] = 0
                                trial_data['want'] = current_choice
                                trial_data['prep_time'] = first_resp['time'] if first_resp['time'] else 0
                                trial_data['resp'] = keys.index(first_resp['key']) if first_resp['key'] else -1
                                trial_data['correct'] = int(correct)
                                trial_data['nogo_this'] = int(current_choice in ignore)
                                writer.writerow(trial_data)
                        else:
                            stim_list[current_choice].draw()
                    else:
                        if trial_timer.getTime() + interval >= 1.0:
                            state = 'post'
                            trial_timer.reset(0)
                            stim_list[current_choice].color = (1, 1, 1)
                            with open(data_path, 'a') as f:
                                writer = csv.DictWriter(f, fieldnames=trial_data.keys())
                                trial_data['req_prep_time'] = 0
                                trial_data['want'] = current_choice
                                trial_data['prep_time'] = first_resp['time'] if first_resp['time'] else 0
                                trial_data['resp'] = keys.index(first_resp['key']) if first_resp['key'] else -1
                                trial_data['correct'] = int(correct)
                                trial_data['nogo_this'] = int(current_choice in ignore)
                                writer.writerow(trial_data)
                        else:
                            stim_list[current_choice].draw()
                else:  # not nogo
                    # if wrong, wait for 1s and go back into trial
                    # if right, show happy feedback for 300ms and go into post
                    if correct:
                        if trial_timer.getTime() + interval >= 0.3:
                            state = 'post'
                            trial_timer.reset(0)
                            stim_list[current_choice].color = (1, 1, 1)
                            with open(data_path, 'a') as f:
                                writer = csv.DictWriter(f, fieldnames=trial_data.keys())
                                trial_data['req_prep_time'] = 0
                                trial_data['want'] = current_choice
                                trial_data['prep_time'] = first_resp['time']
                                trial_data['resp'] = keys.index(first_resp['key'])
                                trial_data['correct'] = int(current_choice == trial_data['resp'])
                                trial_data['nogo_this'] = int(False)
                                writer.writerow(trial_data)
                        else:
                            stim_list[current_choice].draw()
                    else:
                        if trial_timer.getTime() + interval >= 1.0:
                            # timing isn't good for restarting, but it's ok
                            state = 'trial'
                            stim_list[current_choice].color = (1, 1, 1)
                            stim_list[current_choice].draw()
                            win.callOnFlip(empty_trial_resp, trial_resps)
                        else:
                            stim_list[current_choice].draw()
            else:  # forced resp
                # check timing and correctness
                if trial_timer.getTime() + interval >= 0.3:
                    with open(data_path, 'a') as f:
                        writer = csv.DictWriter(f, fieldnames=trial_data.keys())
                        trial_data['req_prep_time'] = current_pt
                        trial_data['want'] = current_choice
                        if first_resp['key']:
                            trial_data['prep_time'] = first_resp['time'] - trial_data['prep_time']
                            trial_data['resp'] = keys.index(first_resp['key'])
                            trial_data['correct'] = int(correct)
                            trial_data['nogo_this'] = int(current_choice in ignore)
                        else:
                            trial_data['resp'] = -1  # forgive me for this transgression (figure it's easier to handle than None)
                            trial_data['correct'] = int(correct)
                            trial_data['prep_time'] = last_beep_time - trial_data['prep_time']
                            trial_data['nogo_this'] = int(current_choice in ignore)
                        writer.writerow(trial_data)

                    state = 'post'
                    trial_timer.reset(0)
                    stim_list[current_choice].color = (1, 1, 1)
                else:
                    stim_list[current_choice].draw()

        if state == 'post':
            if trial_timer.getTime() + interval >= 0.2:
                trial_timer.reset(0)
                coin.stop()
                state = 'pretrial'  # next trial

        if state != 'intro':
            push_feedback.draw()
        win.flip()
        if event.getKeys(['esc', 'escape']):
            break

    # clean up
    keyboard.stop()
    del stim_list
    del intro_text
    win.close()
    core.quit()
