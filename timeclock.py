import time


class TimeClock(object):
    def __init__(self):
        self.reset()

    def getTime(self):
        return time.time() - self.ref

    def reset(self):
        self.ref = time.time()
