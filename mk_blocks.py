from numpy.random import RandomState
from scipy.optimize import brute
import pandas as pd


def f(x, desired_val, interval):
    return abs(interval * x[0] - desired_val)


def mk_table(trials=100, n_options=4,
             t_min=0.05, t_max=0.8,
             seed=1, interval=1/144):

    rng = RandomState(seed=seed)

    min_frames = int(brute(f, (slice(1, 1000, 1),), args=(t_min, interval), finish=None))
    max_frames = int(brute(f, (slice(1, 1000, 1),), args=(t_max, interval), finish=None))
    fin = []

    # trials per final state
    for i in range(n_options):
        # total number of trials for the given state
        n_trials = round(1/n_options * trials)
        for k in range(n_trials):
            fin.append({'choice': i,
                        'prep_time': rng.randint(min_frames, max_frames + 1) * interval})
    rng.shuffle(fin)
    for i in range(n_options):
        fin.insert(0, {'choice': i,
                       'prep_time': 1.2})
    return fin
